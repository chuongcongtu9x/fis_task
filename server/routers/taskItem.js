import express from 'express';
import { createTaskItem, getTaskItem, updateTaskItem } from '../controllers/taskItem.js';

const router = express.Router();

router.get('/', getTaskItem);

router.post('/', createTaskItem);

router.post('/update', updateTaskItem);

export default router;
