import express from 'express';
import { createBoard, getBoards, getTasks, updateBoard } from '../controllers/board.js';
import verifyToken from '../middlewares/auth.js';

const router = express.Router();

router.get('/', getBoards);

router.get('/task', getTasks);

router.post('/', createBoard);

router.post('/update', updateBoard);

export default router;
