import express from 'express';
import { createTask, getTasks, updateTask } from '../controllers/task.js';

const router = express.Router();

router.get('/', getTasks);

router.post('/', createTask);

router.post('/update', updateTask);

export default router;
