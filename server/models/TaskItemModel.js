import mongoose from 'mongoose';

const schema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true
    },
    description: {
      type: String
    },
    boardId: {
        type: String,
        required: true
    },
    taskId: {
        type: String,
        required: true
    },
    position: {
        type: Number,
        required: true
    },
    status: {
      type: Number
    },
    priority: {
      type: Number
    },
    reporterId: {
      type: String,
      required: true
    },
    userId: {
      type: Array,
      required: true,
    }
  },
  { timestamps: true }
);

export const TaskItemModel = mongoose.model('TaskItem', schema);
