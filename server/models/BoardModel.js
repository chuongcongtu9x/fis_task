import mongoose from 'mongoose';

const schema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
    },
    description: {
      type: String,
    },
    isDelete: {
      type: Boolean
    }
  },
  { timestamps: true }
);

export const BoardModel = mongoose.model('Board', schema);
