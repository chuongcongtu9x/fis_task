const board = [
    {
        id: 1,
        title: 'Board 1',
    },
    {
        id: 2,
        title: 'Board 2'
    }
];

const tasks = [
    {
        id: 1,
        boardId: 1,
        title: 'task 1',
        order: 1
    },
    {
        id: 2,
        boardId: 1,
        title: 'task 2',
        order: 2
    },
    {
        id: 3,
        boardId: 1,
        title: 'task 3',
        order: 3
    },
    {
        id: 4,
        boardId: 1,
        title: 'task 4',
        order: 4
    },
    {
        id: 5,
        boardId: 2,
        title: 'task 5',
        order: 1
    },
    {
        id: 6,
        boardId: 2,
        title: 'task 6',
        order: 2
    },
    {
        id: 7,
        boardId: 2,
        title: 'task 7',
        order: 3
    }
];

const taskItems = [
    
]

