import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { GoogleLoginProvider, SocialAuthService, SocialUser } from 'angularx-social-login';
import { LoginInput } from '../../models/LoginInput';
import { AuthService } from '../../services/auth.service';
import { TestService } from '../../services/testapi.service';
import { login, selectToken } from '../../store/auth';
import { State } from '../../store/reducer';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {
  socialUser: SocialUser;
  isLoggedin: boolean;
  userName: string = '';
  password: string = '';
  constructor(
    private socialAuthService: SocialAuthService,
    private authService: AuthService,
    private testService: TestService,
    private store: Store<State>
  ) {

  }
  ngOnInit() {
    this.store.select(selectToken).subscribe(res => {
      console.log(res)
    })
    this.socialAuthService.authState.subscribe((user) => {
      this.socialUser = user;
      this.isLoggedin = (user != null);
      console.log(this.socialUser);
    });
  }

  submit() {
    var value = new LoginInput();
    value.password = this.password;
    value.username = this.userName;

    this.store.dispatch(login({ value }));



    // this.authService.login(value).subscribe(res => {
    //   if (res.success) {
    //     localStorage.setItem(
    //       'user_token',
    //       res.accessToken
    //     )
    //   }
    // })

    // this.testService.getPosts().subscribe(res => {
    //   console.log(res);
    // })


  }

  loginWithGoogle(): void {
    this.store.select(selectToken).subscribe(res => {
      console.log(res)
    })
    // this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  logOut(): void {
    this.socialAuthService.signOut();
  }
}
