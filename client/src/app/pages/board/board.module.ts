import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoardComponent } from './board.component';
import { BoardDndComponent } from '../../components/board-dnd/board-dnd.component';
import { BoardRoutingModule } from './board-routing.module';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule } from '@angular/forms';
import { CreateOrEditTaskItemModalComponent } from '../../components/board-dnd/create-or-edit-task-item-modal/create-or-edit-task-item-modal.component';

@NgModule({
  imports: [
    CommonModule,
    BoardRoutingModule,
    DragDropModule,
    ModalModule.forRoot(),
    FormsModule
  ],
  declarations: [BoardComponent, BoardDndComponent, CreateOrEditTaskItemModalComponent]
})
export class BoardModule { }
