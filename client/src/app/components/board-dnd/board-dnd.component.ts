import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs';
import { createTask, getBoard, getTask, selectBoard, selectTask } from '../../store/board';
import { BoardService } from '../../store/board/board.service';
import { State } from '../../store/reducer';

@Component({
  selector: 'app-board-dnd',
  templateUrl: './board-dnd.component.html',
  styleUrls: ['./board-dnd.component.scss']
})
export class BoardDndComponent implements OnInit {
  @ViewChild('myModal') public myModal: ModalDirective;
  tasks: any[] = [];
  taskTitle;
  taskDes;
  board;
  task$: Observable<any[]>;

  constructor(private store: Store<State>) {
    this.store.dispatch(getBoard());
    this.store.dispatch(getTask());
  }

  ngOnInit() {
    this.store.select(selectBoard).subscribe(res => {
      console.log(res)
      this.board = res;
    })

    this.store.select(selectTask).subscribe(res => {
      console.log(res);
      this.tasks = JSON.parse(JSON.stringify(res ? res : ''));
      console.log(this.tasks);
    })
  }

  drop(event: CdkDragDrop<string[]>) {
    const issue = { ...event.item.data };
    const issues = [...event.container.data];

    console.log(issue);
    console.log(issues);
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      //update position
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
      // update issue
    }
  }

  createTask() {
    const task = {
      title: this.taskTitle,
      description: this.taskDes,
      boardId: this.board[0]._id,
      position: 2
    }

    this.store.dispatch(createTask({ task }));
  }

  modalSave(value) {
    console.log(123)
    this.store.select(selectTask).subscribe(res => {
      console.log(res);
      this.tasks = JSON.parse(JSON.stringify(res ? res : ''));
      console.log(this.tasks);
    })
  }
}
