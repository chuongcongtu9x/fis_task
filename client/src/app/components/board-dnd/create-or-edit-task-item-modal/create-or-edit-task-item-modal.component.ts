import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { BoardService } from '../../../store/board/board.service';
import { State } from '../../../store/reducer';

@Component({
  selector: 'app-create-or-edit-task-item-modal',
  templateUrl: './create-or-edit-task-item-modal.component.html',
  styleUrls: ['./create-or-edit-task-item-modal.component.scss']
})
export class CreateOrEditTaskItemModalComponent implements OnInit {
  @ViewChild('taskItemModal') public taskItemModal: ModalDirective;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  task;
  taskItemTitle;
  taskItemDes;

  constructor(private boardService: BoardService, private store: Store<State>) { }

  ngOnInit() {
  }

  show(task: any) {
    console.log(task);
    this.task = task;
    this.taskItemModal.show();
  }

  createTaskItem() {
    const taskItem = {
      title: this.taskItemTitle,
      description: this.taskItemDes,
      boardId: this.task.boardId,
      taskId: this.task.taskId,
      position: 1,
      status: 1,
      priority: 1,
      reporterId: 1,
      userId: [1,2]
    };

    console.log(this.task.taskId)

    this.boardService.createTaskItem(taskItem).subscribe(res => {
      this.taskItemModal.hide();
      this.modalSave.emit(null);
    })
  }
}
