import { createAction, props } from '@ngrx/store';

// Get board
export const getBoard = createAction(
    '[Board] getBoard');

export const getBoardSuccess = createAction(
    '[Board] getBoard__SUCCESS',
    props<{ board: any }>(),
);

export const getBoardFailure = createAction(
    '[Board] getBoard__FAILURE',
    props<{ error: string }>());

// Get task
export const getTask = createAction(
    '[Board] getTask');

export const getTaskSuccess = createAction(
    '[Board] getTask__SUCCESS',
    props<{ task: any }>(),
);

export const getTaskFailure = createAction(
    '[Board] getTask__FAILURE',
    props<{ error: string }>());

//Create task
export const createTask = createAction(
    '[Board] createTask', props<{ task: any }>());

export const createTaskSuccess = createAction(
    '[Board] createTask__SUCCESS',
    props<{ response: any }>(),
);

export const createTaskFailure = createAction(
    '[Board] createTask__FAILURE',
    props<{ error: string }>());