import { Action, createReducer, on } from '@ngrx/store';
import { takeLast } from 'rxjs/operators';
import { createTask, createTaskFailure, createTaskSuccess, getBoard, getBoardFailure, getBoardSuccess, getTask, getTaskFailure, getTaskSuccess } from '.';

export const BOARD_FEATURE_KEY = 'board';

export interface BoardState {
  board: any;
  loading: boolean;
  task: any;
}

const initialState: BoardState = {
  board: undefined,
  loading: false,
  task: []
};

const _boardReducer = createReducer(
  initialState,
  on(getBoard, state => ({ ...state, loading: true })),
  on(getBoardSuccess, (state, { board }) => ({ ...state, loading: false, board })),
  on(getBoardFailure, (state, { error }) => ({ ...state, loading: false, error })),

  on(getTask, state => ({ ...state, loading: true })),
  on(getTaskSuccess, (state, { task }) => ({ ...state, loading: false, task })),
  on(getTaskFailure, (state, { error }) => ({ ...state, loading: false, error })),

  on(createTask, state => {
    console.log(state);
    return ({ ...state, loading: true });
  }),
  on(createTaskSuccess, (state, { response }) => {
    console.log(response);

    return ({
      ...state,
      task: [...state.task, response],
      loading: false,
    });
  }),
  on(createTaskFailure, (state, { error }) => ({ ...state, loading: false, error })),
);

export function reducer(state: BoardState | undefined, action: Action) {
  return _boardReducer(state, action);
}