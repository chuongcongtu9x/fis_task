import { createFeatureSelector, createSelector } from '@ngrx/store';
import { BoardState, BOARD_FEATURE_KEY } from '.';

const selectBoardState = createFeatureSelector<BoardState>(BOARD_FEATURE_KEY);

export const selectBoard = createSelector(selectBoardState, state => state.board);

export const selectTask = createSelector(selectBoardState, state => state.task);
export const selectLoading = createSelector(selectBoardState, state => state.loading);

