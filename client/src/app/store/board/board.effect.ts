import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, exhaustMap, map, tap } from 'rxjs/operators';
import { createTask, createTaskFailure, createTaskSuccess, getBoard, getBoardFailure, getBoardSuccess, getTask, getTaskFailure, getTaskSuccess } from '.';

import { BoardService } from './board.service';

@Injectable()
export class BoardEffects {
    getBoardEffect$ = createEffect(() =>
        this.action$.pipe(
            ofType(getBoard),
            exhaustMap(() =>
                this.boardService.getBoard().pipe(
                    map(response => getBoardSuccess({ board: response })),
                    catchError(err => of(getBoardFailure({ error: err }))),
                ),
            ),
        ),
    );

    // getBoardSuccessEffect$ = createEffect(
    //     () =>
    //         this.action$.pipe(
    //             ofType(getBoardSuccess),
    //             tap(() => {}),
    //         ),
    //     {
    //         dispatch: false,
    //     },
    // );

    getTaskEffect$ = createEffect(() =>
        this.action$.pipe(
            ofType(getTask),
            exhaustMap(() =>
                this.boardService.getTask().pipe(
                    map(response => getTaskSuccess({ task: response })),
                    catchError(err => of(getTaskFailure({ error: err }))),
                ),
            ),
        ),
    );

    createTaskEffect$ = createEffect(() =>
        this.action$.pipe(
            ofType(createTask),
            exhaustMap(({ task }) =>
                this.boardService.createTask(task).pipe(
                    map(response => createTaskSuccess({ response })),
                    catchError(err => of(createTaskFailure({ error: err }))),
                ),
            ),
        ),
    );

    constructor(
        private readonly action$: Actions,
        private readonly boardService: BoardService,
    ) { }
}