import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { BoardEffects, BOARD_FEATURE_KEY, reducer } from '.';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(BOARD_FEATURE_KEY, reducer),
    EffectsModule.forFeature([BoardEffects])
  ],
})
export class BoardFeatureStoreModule {}