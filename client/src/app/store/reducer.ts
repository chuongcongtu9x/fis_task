import * as fromAuth from './auth';
import { ActionReducerMap } from '@ngrx/store';
import { AUTH_FEATURE_KEY } from './auth';
import { BOARD_FEATURE_KEY } from './board';

export interface State {
  [AUTH_FEATURE_KEY]: fromAuth.AuthState;
  [BOARD_FEATURE_KEY]: fromAuth.AuthState;
}

export const appReducer: ActionReducerMap<State> = {
  [AUTH_FEATURE_KEY]: fromAuth.reducer,
  [BOARD_FEATURE_KEY]: fromAuth.reducer
};