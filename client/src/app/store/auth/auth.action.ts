import { createAction, props } from '@ngrx/store';
import { LoginInput } from '../../models/LoginInput';

export const login = createAction(
    '[Auth] Login',
    props<{ value: LoginInput }>());

export const loginSuccess = createAction(
    '[Auth] Login__SUCCESS',
    props<{ user: any; token: string }>(),
);

export const loginFailure = createAction(
    '[Auth] Login__FAILURE',
    props<{ error: string }>());