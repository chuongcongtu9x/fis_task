import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, exhaustMap, map, tap } from 'rxjs/operators';

import { login, loginFailure, loginSuccess } from './auth.action';
import { AuthService } from './auth.service';

@Injectable()
export class AuthEffects {
    loginEffect$ = createEffect(() =>
        this.action$.pipe(
            ofType(login),
            exhaustMap(({ value }) =>
                this.authService.login(value).pipe(
                    map(response => loginSuccess({ user: response.user, token: response.token })),
                    catchError(err => of(loginFailure({ error: err }))),
                ),
            ),
        ),
    );

    loginSuccessEffect$ = createEffect(
        () =>
            this.action$.pipe(
                ofType(loginSuccess),
                tap(() => this.router.navigate(['/'])),
            ),
        {
            dispatch: false,
        },
    );

    constructor(
        private readonly action$: Actions,
        private readonly authService: AuthService,
        private readonly router: Router,
    ) { }
}