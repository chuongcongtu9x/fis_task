import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LoginInput } from '../../models/LoginInput';

@Injectable({
    providedIn: 'root',
})
export class AuthService {
    readonly APIUrl = 'http://localhost:5000';

    constructor(private http: HttpClient) { }

    httpOptxions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };

    getPosts(): Observable<any[]> {
        return this.http.get<any>(
            `${this.APIUrl}/posts`
        );
    }

    login(val: LoginInput): Observable<any> {
        return this.http.post<any>(this.APIUrl + '/api/auth/login', val);
    }
}
