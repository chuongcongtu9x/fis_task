import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TestService {
  readonly APIUrl = 'http://localhost:5000';

  constructor(private http: HttpClient) { }

  auth_token = localStorage.getItem('user_token');

  headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${this.auth_token}`
  })

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json',
    'Authorization': `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MTZhNGUzNGIzNjkyMTI3ZDQ2ZDM5OTciLCJpYXQiOjE2MzQzNjA3MDl9.0bq_QqUlRApuzDnsH86vM4wmYfWTEX5xCevKn-h9QAM` })
  };

  getPosts(): Observable<any[]> {

    console.log(this.auth_token)
    return this.http.get<any>(
      `${this.APIUrl}/api/posts`, { headers: this.headers }
    );
  }
}
``